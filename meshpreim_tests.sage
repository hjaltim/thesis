def avoids_all(p, mesh_patts):
    return all( mp.avoided_by(p) for mp in mesh_patts )

def hypo_perms(mp, n, ssm=None):
    if not ssm:
        ssm = meshpreimS(mp)
        print "Number of Mesh Patterns yo", len(ssm)
    return sorted( p for p in Permutations(n) if avoids_all(p, ssm) )

def correct_perms(mp, n):
    return sorted( p for p in Permutations(n) if mp.avoided_by(stack_sort(p)) )

def test_correctness(mp, n, ssm=None):
    return correct_perms(mp, n) == hypo_perms(mp, n, ssm)

def test_all(patt_len, perm_len):
    for minkur in permutations([1 .. patt_len]):
        for i in [0 .. patt_len]:
            for j in [0 .. patt_len]:
                mp = MeshPattern(minkur,[(i,j)])
                print "Testing: ", mp
                ssm = meshpreimS(mp)
                #for a in ssm:
                #    show(a,figsize=3)
                print "Number of patterns:", len(ssm)
                for hamstur in [1 .. perm_len]:
                    if not test_correctness(mp,hamstur,ssm):
                        print "Epic FAIL"
                        print mp
                        print "length of perm", hamstur
                        print "-"*40

def visual_test(mp,multiple=False,width=5,scale=0.45):
    print "Original pattern:"
    if multiple:
        show_multiple([mp],width,scale)
    else:
        show(mp)
    print "-"*40
    print "Stack Machine"
    print "-"*40
    sm = preimS(mp)
    for p in sm:
        print "Original:"
        if multiple:
            show_multiple([p],width,scale)
        else:
            show(p)
        print "Expanded:"
        if multiple:
                show_multiple(p.expand(),width,scale)
        else:
            for exp in p.expand():
                show(exp)

        print "-"*40
        print "Shaded Stack Machine"
        print "-"*40

        if multiple:
            res = mdecorateS(mp,p)
            if res:
                show_multiple(res,width,scale)
            else:
                print '#'*40
                print 'No shaded candidates'
                print '#'*40
        else:
            for p in meshpreimS(mp):
                show(p)
