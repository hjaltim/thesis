import datetime

load("http://dl.dropbox.com/u/11524527/Sage_code/sorting_functions.sage")
load("http://dl.dropbox.com/u/11524527/Sage_code/stack_machines.sage")
load("http://dl.dropbox.com/u/11524527/Sage_code/pattern_classes.sage")
load("http://dl.dropbox.com/u/11524527/Sage_code/GRIM.sage")
load("http://dl.dropbox.com/u/11524527/Sage_code/GRIM_subfunctions.sage")
load("http://dl.dropbox.com/u/11524527/Sage_code/QuickSort.py")
load("http://dl.dropbox.com/u/11524527/Sage_code/Q.py")
load("http://dl.dropbox.com/u/11524527/Sage_code/basic_functions.sage")
load("http://dl.dropbox.com/u/11524527/Sage_code/sorting_method_tests.sage")

min_fun = 1
max_fun = 1

res = main_test(min_fun,max_fun,report=True)
save(res,'%s-min%d-max%d.sobj'%(datetime.datetime.now().strftime("%Y-%m-%d"),min_fun,max_fun))
show_results(res, display = False, show_mesh = False, show_insufficient=False)