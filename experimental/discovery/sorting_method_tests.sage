def complement(perm):
	return [len(perm) - i + 1 for i in perm]
	
def inverse(perm):
	return [ i for _,i in sorted( zip(perm, (1..)) ) ]

def reverse(perm):
	return perm[::-1]
	
def complement_reverse(perm):
	return complement(reverse(perm))
	
def complement_inverse(perm):
	return complement(inverse(perm))
	
def inverse_complement(perm):
	return inverse(complement(perm))

def complement_reverse_inverse(perm):
	return complement(reverse(inverse(perm)))

def comp(funs):
	if not funs:
		return lambda x: x
	return lambda x: comp(funs[:-1])(funs[-1](x))
	
def get_name(funs):
	return ' o '.join(map(lambda x: x.func_name,funs))



symmetries = [
ident,
reverse,
complement, 
inverse,
complement_reverse,
complement_inverse,
inverse_complement,
complement_reverse_inverse
]

def strand_with_bubble(perm):
	return StS(perm, subsort=bubble_sort)
	
def strand_with_insertion(perm):
	return StS(perm, subsort=IS)
	
def strand_with_pancakes(perm):
	return StS(perm, subsort=PS_rec)
	
def stack_depth_2(perm):
	return stack_sort_depth(2,perm)


sorting_methods = [
Q,
stack_sort,
StR,
bubble_sort,
US,
IS,
SS,
USR,
PS,
strand_with_bubble,
strand_with_insertion,
strand_with_pancakes,
stack_depth_2,
DnS,
pop_stack_sort
]

goodpatts = dict()
A = dict()
def test_funcs(fun, M = 4, N = 6, description = 6):
	"""
	M - Maximum length of patterns to search for
	N - Maximum length of permutations to consider
	"""
	global A
	A = perms_sat_prop(N,lambda x: fun(x) == range(1, len(x)+1))
	
	sizes = [ len(v) for k,v in sorted(A.items()) ]
	SG = GRIM(M,N,report=False)
	suffice = patterns_suffice(SG, description, A, compare=True, report=not True)
	
	all_mps = []
	shaded = False
	for le,mps in sorted(SG.items()):
		for patt,shads in mps.items():
			for sh in shads:
				if sh:
					shaded = True
				all_mps.append(MeshPattern(patt,sh))

	# niceness of the result
	# 1: Were the discovered patterns sufficient for describing
	#    the property for permutations of length up to and including N
	# 2: Are all the patterns classical
	# 3: Number of discovered patterns
	# 4: The discovered patterns
	niceness = ( suffice, not shaded, len(all_mps), all_mps )
		
	return niceness

def test_compare(lef, rig, M = 4, N = 6, description = 6):
	"""
	M - Maximum length of patterns to search for
	N - Maximum length of permutations to consider
	"""
	global A
	A = perms_sat_prop(N, lambda x: lef(x) == rig(x))
	
	sizes = [ len(v) for k,v in sorted(A.items()) ]
	SG = GRIM(M,N,report=False)
	suffice = patterns_suffice(SG, description, A, compare=True, report=not True)
	
	all_mps = []
	shaded = False
	for le,mps in sorted(SG.items()):
		for patt,shads in mps.items():
			for sh in shads:
				if sh:
					shaded = True
				all_mps.append(MeshPattern(patt,sh))

	# niceness of the result
	# 1: Were the discovered patterns sufficient for describing
	#    the property for permutations of length up to and including N
	# 2: Are all the patterns classical
	# 3: Number of discovered patterns
	# 4: The discovered patterns
	niceness = ( suffice, not shaded, len(all_mps), all_mps )
		
	return niceness


def comparison_test(M = 4, N = 6, description = 6, report=False):
	"""
	min_fun: The minimum number of sorting functions to compose
	max_fun: The maximum number of sorting functions to compose
	"""
	results_dict = {}
	for i in range(1,2): # left dude
		for j in range(1,2): # right dude
			for left_sort in Words(sorting_methods,i):
				for right_sort in Words(sorting_methods,j):
					for sym in Words(symmetries, i+1):
						funs = flatten(zip(sym,left_sort) + [sym[-1]])
						res = test_compare(comp(funs),comp(right_sort), M, N, description)
						results_dict['%s = %s'%(get_name(funs),get_name(right_sort))] = res
						if report:
							print 'The comparison: %s = %s'%(get_name(funs),get_name(right_sort))
							print 'Yielded %d patterns'%res[2]
							if res[0]:
								print 'Patterns suffice!'
							else:
								print 'Patterns did not suffice :('
							if res[1]:
								print 'Contains only classical patterns!'
								if res[2] < 8:
									print ', '.join(map(lambda x: ''.join(map(str,list(x))),res[3]))
							else:
								print 'Contains mesh-y patterns :('
							print '-'*60
							print
	return (M, N, description, results_dict)

def main_test(min_fun = 1, max_fun = 2, M = 4, N = 6, description = 6, report=False):
	"""
	min_fun: The minimum number of sorting functions to compose
	max_fun: The maximum number of sorting functions to compose
	"""
	results_dict = {}
	for i in range(min_fun,max_fun+1):
		for sort in Words(sorting_methods,i):
			for sym in Words(symmetries, i-1):
				funs = flatten(zip(sort,sym)+[sort[-1]])
				res = test_funcs(comp(funs), M, N, description)
				results_dict[get_name(funs)] = res
				if report:
					print 'The sorting method: %s'%get_name(funs)
					print 'Yielded %d patterns'%res[2]
					if res[0]:
						print 'Patterns suffice!'
					else:
						print 'Patterns did not suffice :('
					if res[1]:
						print 'Contains only classical patterns!'
						if res[2] < 8:
							print ', '.join(map(lambda x: ''.join(map(str,list(x))),res[3]))
					else:
						print 'Contains mesh-y patterns :('
					print '-'*60
					print
	return (M, N, description, results_dict)

def main_test2(min_fun = 1, max_fun = 2, M = 4, N = 6, description = 6, report=False):
	"""
	min_fun: The minimum number of sorting functions to compose
	max_fun: The maximum number of sorting functions to compose
	"""
	results_dict = {}
	for i in range(min_fun,max_fun+1):
		for sort in Words(sorting_methods,i):
			if pop_stack_sort not in sort:
				continue
			for sym in Words(symmetries, i-1):
				funs = flatten(zip(sort,sym)+[sort[-1]])
				res = test_funcs(comp(funs), M, N, description)
				results_dict[get_name(funs)] = res
				if report:
					print 'The sorting method: %s'%get_name(funs)
					print 'Yielded %d patterns'%res[2]
					if res[0]:
						print 'Patterns suffice!'
					else:
						print 'Patterns did not suffice :('
					if res[1]:
						print 'Contains only classical patterns!'
						if res[2] < 8:
							print ', '.join(map(lambda x: ''.join(map(str,list(x))),res[3]))
					else:
						print 'Contains mesh-y patterns :('
					print '-'*60
					print
	return (M, N, description, results_dict)

def show_results(results, display = False, show_mesh = True, show_insufficient = False, condition = lambda x: True, cutoff = Infinity):
	print 'Maximum pattern length: %d'%results[0]
	print 'Maximum permutation length: %d'%results[1]
	print 'Verified up to length: %d'%results[2]
	print
	print 'And here are the results of the Icelandic vote'
	count  = 0
	res_dict = results[-1]
	for name, res in sorted(res_dict.items(), key = lambda x: (not x[1][0], not x[1][1], x[1][2])):
		if count > cutoff:
			print 'Cutoff point reached'
			break
		if condition(res):
			suff, cl, count, patts = res
			if not show_insufficient and not suff:
				break
			if not show_mesh and not cl:
				continue
			print name
			if show_insufficient:
				if suff:
					print 'Sufficient!'
				else:
					print 'Insufficient :('
			if display and patts:
				show_multiple(patts, 5, .4)
			else:
				print patts
			print '-' * 60
			print
			count += 1


unknown = [
	[[4, 2, 3, 1], [3, 2, 1, 4]],
	[[4, 2, 1, 3], [1, 4, 3, 2]],
	[[4, 3, 1, 2], [3, 4, 2, 1]],
	[[4, 2, 1, 3], [2, 4, 3, 1]],
	[[4, 3, 1, 2], [3, 1, 2, 4]],
	[[4, 3, 1, 2], [3, 2, 1, 4]],
	[[4, 2, 1, 3], [2, 1, 4, 3]],
	[[4, 2, 1, 3], [3, 4, 2, 1]],
	[[4, 3, 2, 1], [3, 2, 1, 4]],
	[[4, 2, 3, 1], [4, 1, 2, 3]],
	[[4, 3, 2, 1], [4, 2, 1, 3]],
	[[4, 1, 2, 3], [3, 4, 1, 2]],
	[[3, 4, 1, 2], [2, 4, 1, 3]],
	[[4, 3, 2, 1], [4, 2, 3, 1]]
]
