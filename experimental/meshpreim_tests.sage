def c(*args):
    if not args:
        return lambda x: x
    else:
        return lambda x: c(*args[:-1])(args[-1](x))

def rev(perm):
    return perm[::-1]

def inv(perm):
    return [ a for _,a in sorted(zip(perm,(1..))) ]

def comp(perm):
    return [ len(perm) + 1 - a for a in perm ]

def is_ascending(perm):
    if len(perm) < 2:
        return True
    return all(a < b for a,b in zip(perm,perm[1:]))

def test_all(patt_len, perm_len):
    for minkur in permutations([1 .. patt_len]):
        for i in [0 .. patt_len]:
            for j in [0 .. patt_len]:
                mp = MeshPattern(minkur,[(i,j)])
                print "Testing: ", mp
                ssm = meshpreimS(mp)
                #for a in ssm:
                #    show(a,figsize=3)
                print "Number of patterns:", len(ssm)
                for hamstur in [1 .. perm_len]:
                    if not test_correctness(mp,hamstur,ssm):
                        print "Epic FAIL"
                        print mp
                        print "length of perm", hamstur
                        print "-"*40

def visual_test(mp,multiple=False,width=5,scale=0.45):
    print "Original pattern:"
    if multiple:
        show_multiple([mp],width,scale)
    else:
        show(mp)
    print "-"*40
    print "Stack Machine"
    print "-"*40
    sm = preimS(mp)
    for p in sm:
        print "Original:"
        if multiple:
            show_multiple([p],width,scale)
        else:
            show(p)
        print "Expanded:"
        if multiple:
                show_multiple(p.expand(),width,scale)
        else:
            for exp in p.expand():
                show(exp)

        print "-"*40
        print "Shaded Stack Machine"
        print "-"*40

        if multiple:
            res = mdecorateS(mp,p)
            if res:
                show_multiple(res,width,scale)
            else:
                print '#'*40
                print 'No shaded candidates'
                print '#'*40
        else:
            for p in meshpreimS(mp):
                show(p)


def para_test_correctness3(mp, n, cpus=None):
    ssm = meshpreimS(mp)
    return para_perms_stack_test(n, mp, ssm, cpus)

def para_test_correctness2(mp, n, cpus=None):
    ssm = meshpreimS(mp)
    prop = lambda p: mp.avoided_by(stack_sort(p)) != avoids_all(p, ssm)
    return para_perms_sat_prop(n, prop, cpus)

def para_test_correctness(mp, n, cpus=None):
    return para_correct_perms(mp, n, cpus // 2) == para_hypo_perms(mp, n, cpus // 2)

def para_correct_perms(mp, n, cpus=None):
    prop = lambda x: mp.avoided_by(stack_sort(x))
    return sorted( para_perms_sat_prop(n,prop,cpus) )

def para_hypo_perms(mp, n, cpus=None):
    ssm = meshpreimS(mp)
    print "Number of Mesh Patterns yo", len(ssm)
    return sorted( para_perms_avoiding_many(n,ssm,cpus) )

def test_correctness(mp, n, ssm=None):
    return correct_perms(mp, n) == hypo_perms(mp, n, ssm)

def correct_perms(mp, n):
    return sorted( p for p in Permutations(n) if mp.avoided_by(stack_sort(p)) )

def avoids_all(p, mesh_patts):
    return all( mp.avoided_by(p) for mp in mesh_patts )

def hypo_perms(mp, n, ssm=None):
    if not ssm:
        ssm = meshpreimS(mp)
        print "Number of Mesh Patterns yo", len(ssm)
    return sorted( p for p in Permutations(n) if avoids_all(p, ssm) )



def test_correctness_multi(mp, n):
    return correct_perms(mp, n) == multi_hypo_perms(mp, n)

def multi_hypo_perms(mp, n):
    ssm = []
    for p in mp.shaded_boxes:
        ssm += meshpreimS( MeshPattern( list(mp), [p] ) )
    print "Number of Mesh Patterns yo", len(ssm)
    return sorted( p for p in Permutations(n) if avoids_all(p, ssm) )


# Parallel version of perms_avoiding_many
@parallel
def para_subs_perms_avoiding_many(v,cpus,n,Ms):
    """
    Helper function for the function below
    """

    return filter( lambda x: avoids_all(x,Ms), map( lambda y: Permutations(n)[y], xrange(v,factorial(n),cpus) ) )

def para_perms_avoiding_many(n,Ms,cpus=None):
    """
    Returns a dictionary where the key n points to the permutations in S_n that avoid the mesh patterns in Ms
    """

    if not cpus:
        cpus = sage.parallel.ncpus.ncpus()

    return sum( map( lambda x: x[1], para_subs_perms_avoiding_many( map( lambda y: (y,cpus,n,Ms) , range(cpus) ) ) ), [] )

# Parallel version of perms_sat_prop
@parallel
def para_subs_perms_sat_prop(v,cpus,n,prop):
    all_perms = Permutations(n)
    return filter( prop, map( lambda y: all_perms[y], xrange(v,factorial(n),cpus) ) )

def para_perms_sat_prop(n,prop,cpus=None):
    if not cpus:
        cpus = sage.parallel.ncpus.ncpus()

    return sum( map( lambda x: x[1], list( para_subs_perms_sat_prop( map( lambda y: (y,cpus,n,prop) , range(cpus) ) ) ) ), [] )

# Parallel version of perms_sat_prop
@parallel
def para_perms_stack_test_help(v,cpus,n,mp,ssm):
    all_perms = Permutations(n)
    return filter( lambda p: mp.avoided_by(stack_sort(p)) != avoids_all(p, ssm), map( lambda y: all_perms[y], xrange(v,factorial(n),cpus) ) )

def para_perms_stack_test(n,mp,ssm,cpus=None):
    if not cpus:
        cpus = sage.parallel.ncpus.ncpus()

    return sum( map( lambda x: x[1], list( para_perms_stack_test_help( map( lambda y: (y,cpus,n,mp,ssm) , range(cpus) ) ) ) ), [] )



from datetime import datetime


def para_test_correctness4(mp, n, b=1, cpus=None):
    ssm = meshpreimS(mp)
    all_perms = Permutations(n)
        total_length = factorial(n)
    length = ceil(factorial(n) / b)
    for i in range(b):
        res = stack_test( [ all_perms[j] for j in range(i*length,min(i*length + length,total_length)) ], mp, ssm, cpus)
        if res:
            print "FAILURE"
            print res
            return False
        print datetime.now(), "Iteration %d is doned"%i
    return True

# Parallel version of perms_sat_prop
@parallel
def stack_test_help(v,cpus,perms,mp,ssm):
    len_perms = len(perms)
    return filter( lambda p: mp.avoided_by(stack_sort(p)) != avoids_all(p, ssm), map( lambda y: perms[y], xrange(v,len_perms,cpus) ) )

def stack_test(perms,mp,ssm,cpus=None):
    if not cpus:
        cpus = sage.parallel.ncpus.ncpus()

    return sum( map( lambda x: x[1], list( stack_test_help( map( lambda y: (y,cpus,perms,mp,ssm) , range(cpus) ) ) ) ), [] )


#### TEST CODE FOR STACKS WITH SYMMETRIES ####

def sorts(funcs, perm):
    return is_ascending(c(*funcs)(perm))

def test_correctness_sym(funcs,patts,n):
    return all( sorts(funcs, p) == avoids_all(p, patts) for p in Permutations(n) )
