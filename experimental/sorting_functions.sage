#
# helper function for stack-sort and bubble-sort. finds the location of the maximal element
#

from sage.combinat.permutation import to_standard

def loc_max(w):
    L = max(w)

    for i in [0..L-1]:
        if w[i] == L:
            return i

#
# function takes a permutation w and does one pass of stack-sort on it
#

def stack_sort(w):
    i = len(w)

    if i == 0:
        return []

    if i == 1:
        return w
    else:
        [j,J] = [loc_max(w),max(w)]

        if j == 0:
            W2 = list(stack_sort(w[1:i]))
            W2.append(J)

            return W2

        if j == i-1:
            W1 = list(stack_sort(w[0:i-1]))
            W1.append(J)

            return W1


        else:
            W1 = list(stack_sort(w[0:j]))
            W2 = list(stack_sort(w[j+1:i]))

            W1.extend(W2)
            W1.extend([J])

            return W1

#
# function takes a permutation w and does one pass of bubble-sort on it
#

def stack_sort_depth(d, w):
    res = []
    stack = []
    for a in w:
        if not stack:
            stack.append(a)
        elif stack[-1] > a:
            if len(stack) < d:
                stack.append(a)
            else:
                res.append(a)
        else:
            while stack and stack[-1] < a:
                res.append(stack.pop())
            stack.append(a)
    return res + stack[::-1]


def stack_sort_rec(k,perm):
    if not perm:
        return []

    if not k:
        return perm

    i = loc_max(perm)

    return stack_sort_rec(k,perm[:i]) + stack_sort_rec(k-1,perm[i+1:]) + [perm[i]]

def leftMaxima(w):
	L = w[:1]
	for x in w[1:]:
		if x > L[-1]:
			L.append(x)
	return L

def merge(u,v):
	if not u: return v
	if not v: return u
	if u[0] < v[0]:
		return u[:1] + merge(u[1:], v)
	else:
		return v[:1] + merge(u, v[1:])

def Q(w):
	u = leftMaxima(w)
	v = w[:]
	for x in u:
		v.remove(x)
	return merge(u,v)

def bubble_sort(w):

    i = len(w)

    if i == 0:
        return []

    if i == 1:
        return w
    else:
        [j,J] = [loc_max(w),max(w)]

        if j == 0:
            W2 = w[1:i]
            W2.append(J)

            return W2

        if j == i-1:
            W1 = list(bubble_sort(w[0:i-1]))
            W1.append(J)

            return W1


        else:
            W1 = list(bubble_sort(w[0:j]))
            W2 = w[j+1:i]

            W1.extend(W2)
            W1.extend([J])

            return W1

def IS(w):
    if len(w) < 2:
        return w

    last = w[0]
    for i in range(1,len(w)):
        if w[i] < last:
            ind = i
            break
        last = w[i]
    else:
        return w

    new = w[:ind] + w[ind+1:]

    for i,a in enumerate(new):
        if a > w[ind]:
            return new[:i] + [w[ind]] + new[i:]

def swap(w,i,j):
    cp = w[:]
    cp[i],cp[j] = cp[j],cp[i]
    return cp

def SS(w):
    w = list(w)
    if len(w) < 2:
        return w

    for i,a in enumerate(w):
        if a != i + 1:
            ind = w.index(i+1)
            return swap(w,i,ind)
    return w

def SS2(w):
    """
    Looks like the same stuff as above (i.e. SS)
    """
    w = list(w)
    if len(w) < 2:
        return w

    for i,a in enumerate(w):
        if a != i + 1:
            ind = w.index(i+1)
            w.pop(ind)
            w.insert(i,i+1)
            break
    return w

def ident(p):
    return p

def US(x, d=Infinity):
    if not x or not d:
        return x
    i = x.index(min(x))
    return [x[i]] + US(x[:i],d) + US(x[i+1:],d-1)

def USR(x, d=Infinity):
    if not x or not d:
        return x
    i = x.index(min(x))
    return [x[i]] + US(x[i+1:],d) + US(x[:i],d-1)

def St(x, d=Infinity):
    if not x or not d:
        return x
    i = x.index(max(x))
    return St(x[:i],d) + St(x[i+1:],d-1) + [x[i]]

def StR(x, d=Infinity):
    if not x or not d:
        return x
    i = x.index(max(x))
    return StR(x[i+1:],d) + StR(x[:i],d-1) + [x[i]]

def merge(a,b):
    if not b:
        return a
    if not a:
        return b
    if a[0] < b[0]:
        return [a[0]] + merge(a[1:],b)
    return [b[0]] + merge(a,b[1:])

# Strand sort
def StS(w, subsort=ident):
    w = list(w)
    if len(w) < 2:
        return w
    last = w.pop(0)
    run = [last]
    i = 0
    while i < len(w):
        if w[i] > last:
            last = w.pop(i)
            run.append(last)
        else:
            i += 1
    return merge(run,subsort(w))

# Dnarts sort
def DnS(w,subsort=ident):
    w = list(w)
    if len(w) < 2:
        return w
    ind = len(w) - 1
    while w[ind] == ind + 1:
        ind -= 1

    if ind < 0:
        return w

    last = w.pop(ind)
    nur = [last]
    i = ind - 1
    #while i > 0: # Gives interesting results
    while i >= 0:
        if w[i] > last:
            last = w.pop(i)
            nur.append(last)
        i -= 1

    return merge(nur,subsort(w))

def MS(w,subsort=ident):
    if len(w) < 2:
        return w
    n = len(w)
    return merge( subsort(w[:n//2]), subsort(w[n//2:] ) )

def PS(w):
    if len(w) < 2:
        return w

    i = len(w) - 1
    while i >= 0 and w[i] == i + 1:
        i -= 1

    if i < 0:
        return w


    ind = w.index(i+1)

    return w[ind+1:i+1][::-1] + w[:ind] + [i+1] + w[i+1:]

def PS_rec(w):
    if not w:
        return []

    i = loc_max(w)

    return PS_rec(w[:i][::-1]) + PS_rec(w[i+1:]) + [w[i]]

def pop_stack_sort(w):
    res = []
    stack = []
    for a in w:
        if not stack:
            stack.append(a)
        elif stack[-1] > a:
            stack.append(a)
        else:
            while stack:
                res.append(stack.pop())
            stack.append(a)
    return res + stack[::-1]

def seq_stack(w):
    stacks = [[],[]]
    inp = list(w)
    out = []
    while inp:
        curr = inp.pop(0)
        if not stacks[0] or stacks[0][-1] < inp:
            stacks[0].append(curr)
            continue
        else:
            while stacks[0] and stacks[1] and stacks[1][-1] < stacks[0][-1]:
                out.append(stacks[1].pop())
            while stacks[0] and stacks[0][-1] > curr:
                stacks[1].append(stacks[0].pop())
            stacks[0].append(curr)
    out += stacks[0] + stacks[1][::-1]
    return out

def seq_stack(w):
    stacks = [[],[]]
    inp = list(w)
    out = []
    while inp:
        curr = inp.pop(0)
        if not stacks[0] or stacks[0][-1] < inp:
            stacks[0].append(curr)
            continue
        else:
            while stacks[0] and stacks[1] and stacks[1][-1] < stacks[0][-1]:
                out.append(stacks[1].pop())
            while stacks[0] and stacks[0][-1] > curr:
                stacks[1].append(stacks[0].pop())
            stacks[0].append(curr)
    out += stacks[0] + stacks[1][::-1]
    return out
def seq_stack(w):
    stacks = [[],[]]
    inp = list(w)
    out = []
    while inp:
        curr = inp.pop(0)
        if not stacks[0] or stacks[0][-1] < inp:
            stacks[0].append(curr)
            continue
        else:
            while stacks[0] and stacks[1] and stacks[1][-1] < stacks[0][-1]:
                out.append(stacks[1].pop())
            while stacks[0] and stacks[0][-1] > curr:
                stacks[1].append(stacks[0].pop())
            stacks[0].append(curr)
    out += stacks[0] + stacks[1][::-1]
    return out


# Wrong (but interesting) implementation of
# http://www.math.uiuc.edu/~west/regs/stacksort.html
def parall_stack_w(w, no_stacks=2):
    stacks = [[] for _ in range(no_stacks)]
    inp = list(w)
    out = []
    while inp:
        curr = inp.pop(0)
        while(True):
            #print "curr", curr
            #print "stacks", stacks
            #print "inp", inp
            #print "out", out
            if not all(stacks):
                for s in stacks:
                    if not s:
                        s.append(curr)
                        break
                break
            else:
                smallest, ind = sorted( (x[-1], i) for i,x in enumerate(stacks) )[0]
                if curr > smallest:
                    out.append(stacks[ind].pop())
                else:
                    stacks[ind].append(curr)
                    break
    while any(stacks):
        _, ind = sorted( (x[-1], i) for i,x in enumerate(stacks) if x )[0]
        out.append(stacks[ind].pop())
    return out

# http://www.math.uiuc.edu/~west/regs/stacksort.html
def parall_stack(w, no_stacks=2):
    stacks = [[] for _ in range(no_stacks)]
    inp = list(w)
    out = []
    while inp:
        curr = inp.pop(0)
        while(True):
            if not all(stacks):
                for s in stacks:
                    if not s:
                        s.append(curr)
                        break
                break
            else:
                if all(map(lambda x: x[-1] < curr, stacks)):
                    _, ind = min( (x[-1], i) for i,x in enumerate(stacks) )
                    out.append(stacks[ind].pop())
                else:
                    _, ind = min( (x[-1], i) for i,x in enumerate(stacks) if x[-1] > curr )
                    stacks[ind].append(curr)
                    break
    while any(stacks):
        _, ind = sorted( (x[-1], i) for i,x in enumerate(stacks) if x )[0]
        out.append(stacks[ind].pop())
    return out

def av_serial_stacks(w):
    stacks = [[],[]]
    inp = list(w)
    out = []
    while inp:
        curr = inp.pop(0)
        while not Permutation(to_standard((stacks[0]+[curr])[::-1])).avoids([2,3,1]):
            curr2 = stacks[0].pop()
            while stacks[1] and stacks[1][-1] < curr2:
                out.append(stacks[1].pop())
            stacks[1].append(curr2)
        stacks[0].append(curr)

    while stacks[0]:
        curr2 = stacks[0].pop()
        while stacks[1] and stacks[1][-1] < curr2:
            out.append(stacks[1].pop())
        stacks[1].append(curr2)

    out.extend(stacks[1][::-1])
    return out





