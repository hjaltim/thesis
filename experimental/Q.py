def leftMaxima(w):
	L = w[:1]
	for x in w[1:]:
		if x > L[-1]:
			L.append(x)
	return L

def merge(u,v):
	if not u: return v
	if not v: return u
	if u[0] < v[0]:
		return u[:1] + merge(u[1:], v)
	else:
		return v[:1] + merge(u, v[1:])

def Q(w):
	u = leftMaxima(w)
	v = w[:]
	for x in u:
		v.remove(x)
	return merge(u,v)

def smart_Q(w):
	queue = []
	res = []
	
	for a in w:
		if not queue:
			queue.append(a)
		elif queue[-1] < a:
			queue.append(a)
		else:
			while queue[0] < a:
				res.append(queue.pop(0))
			res.append(a)
	
	return res + queue

def find_min(queues):
	min_q, min_elem = None, Infinity
	for q in queues:
		if q and (q[0] < min_elem):
			min_q = q
			min_elem = q[0]
	return min_q, min_elem

def not_so_smart_Qs(count,w):
	queues = [ [] for _ in range(count) ]
	res = []
	
	for a in w:
		for q in queues:
			if not q:
				q.append(a)
				break
		else:
			for q in queues:
				if q[-1] < a:
					q.append(a)
					break
			else:
				min_q, min_elem = find_min(queues)
				while min_elem < a:
					res.append(min_q.pop(0))
					min_q, min_elem = find_min(queues)
				res.append(a)
	
	min_q, min_elem = find_min(queues)
	while min_elem < Infinity:
		res.append(min_q.pop(0))
		min_q, min_elem = find_min(queues)
	
	return res

def smart_Qs(count,w):
	queues = [ [] for _ in range(count) ]
	res = []
	
	for a in w:
		for q in queues:
			if not q or q[-1] < a:
				q.append(a)
				break
		else:
			min_q, min_elem = find_min(queues)
			while min_elem < a:
				res.append(min_q.pop(0))
				min_q, min_elem = find_min(queues)
			res.append(a)
	
	min_q, min_elem = find_min(queues)
	while min_elem < Infinity:
		res.append(min_q.pop(0))
		min_q, min_elem = find_min(queues)
	
	return res