def pairs(w):
    inv  = []
    ninv = []
    for i,u in enumerate(w):
        for v in w[i+1:]:
            if u > v:
                inv.append((u,v))
            else:
                ninv.append((u,v))
    return inv, ninv

def candS(w):
    if w:
        w = w[:]
        n = max(w)
        i = w.index(n)
        w.pop(i)
        return [ x + [n] + y for j in range(i+1) for x in candS(w[:j]) for y in candS(w[j:]) ]
    else:
        return [[]]

def decorateS(target, candidate):
    srt = lambda s: list(sorted(s))
    n = len(target)
    inv, ninv = pairs(target)

    p = dict(zip(candidate, range(1,n+1))) # inverse of candidate

    shades = set( (x,y) for (u,v) in ninv for x in range(p[v],p[u]) for y in range(v,n+1) )

    marks = set([])
    for (u,v) in inv:
        i, j = p[u], p[v]
        if all( y < u for y in candidate[i:j] ):
            M = frozenset( (x,y) for x in range(i,j) for y in range(u,n+1) ) - shades

            if len(M) == 0:
                return None

            check = False

            if not any ( m.issubset(M) for m in marks ):
                check = True

            if check == True:
                newmarks = set([])

                for m in marks:
                    if not M.issubset(m):
                        newmarks.add(m)

                newmarks.add(M)
                marks = newmarks

    return MarkedMeshPattern(candidate, shades, [ (m, 1) for m in marks])

def preimS(*args):
    if len(args) == 1:
        target, = args
        return filter( lambda x: x, [ decorateS(target, candidate) for candidate in candS(target) ] )
    else:
        return sum( map(preimS, args), [] )

def meshpreimS(*args):
    if len(args) == 1:
        target, = args
        if len(target.shaded_boxes) == 0:
            return preimS(target)
        return filter( lambda x: x, [ p for candidate in candS(target) for p in mdecorateS(target, decorateS(target, candidate)) ] )
    else:
        return sum( map(meshpreimS, args), [] )


def mdecorateS(target, cand):
    if not cand:
        return []

    n = len(target)

    res = []

    inv = cand.inverse()

    xs = [a for a,_ in target.shaded_boxes]
    lef, rig = min(xs), max(xs) + 1
    ys = [b for _,b in target.shaded_boxes]
    bot, top = min(ys), max(ys) + 1

    f_x1 = 0
    if lef > 0:
        f_x1 = inv(target[lef - 1])
    f_x2 = n + 1
    if rig < n + 1:
        f_x2 = inv(target[rig - 1])

    for mesh, rect in cand.expand(follow=[(f_x1,bot),(f_x2,top)]):
        res.extend(apply_metapattern(target, mesh, rect))

    return res

def apply_metapattern(target, cand, rect):
    n = len(target)
    k = len(cand)

    (lef,bot), (rig,top) = rect
    xs = [a for a,_ in target.shaded_boxes]
    t_lef, t_rig = min(xs), max(xs) + 1
    ys = [b for _,b in target.shaded_boxes]
    t_bot, t_top = min(ys), max(ys) + 1

    res = []

    # Shaded region is in leftmost column
    if t_lef == 0:

        # Shaded region is below leftmost element in target
        if target[t_rig - 1] >= t_top:
            if cand.rect_contains_elements( [(lef,bot), (rig,top)] ):
                return []
            before_elem = [ (x,y) for x in range(0,rig) for y in range(bot,top) ]

            height = cand[rig - 1]
            leftmost_popper = k

            for i,h in list(enumerate(cand))[rig:]:
                if h > height:
                    leftmost_popper = i
                    break

            for i in [rig .. leftmost_popper]:
                if cand.rect_contains_elements( [(rig,bot), (i+1,top)] ):
                    continue
                sh = before_elem + [ (x,y) for x in [rig .. i] for y in [height .. k+1] + [bot .. top-1] ]
                for j in [height .. k]:
                    if not cand.is_shaded(i,j):
                        m = cand.insert(i,j)
                        res.append( m.add_shading(sh) )

            if not cand.rect_contains_elements( [(rig,bot), (leftmost_popper+1,top)] ):
                sh = before_elem + [ (x,y) for x in [rig..leftmost_popper] for y in [bot .. top-1] + [height .. k] ]
                res.append( cand.add_shading(sh) )

            return res

        # above leftmost element
        else:
            height = cand[rig - 1]

            leftmost_inside = rig - 1
            for i,h in graph(cand)[:rig - 1]:
                if bot < h < top:
                    leftmost_inside = i - 1
                    break

            for i in [0 .. leftmost_inside]:
                for j in range(bot,top):
                    if not cand.is_shaded(i,j):
                        new_cand = cand.insert(i,j)
                        if new_cand.rect_contains_elements( [(i+1,j+1), (rig+1,k+2)] ):
                            continue
                        shading = [ (x,y) for x in [0 .. i] for y in [bot .. top] ]
                        shading += [ (x,y) for x in [i+1 .. rig] for y in [j+1 .. k+1] ]
                        elems = new_cand.get_elements_in_rect( [(i+1,bot), (rig+1,j)] )
                        if not is_descending( graph_inv(elems) ):
                            continue
                        sh_cand = new_cand.add_shading(shading)
                        res.append( sh_cand.add_ascending_restriction( [(i+1,bot), (rig+1,j+1)] ) )

            # There is an element in the dangerzone
            # TODO: Is this possible? Can it come out of preimS?
            if leftmost_inside != rig - 1 and not cand.rect_contains_elements( [(leftmost_inside+1,cand[leftmost_inside]), (rig,k+1)] ):
                shading = [ (x,y) for x in [0 .. leftmost_inside] for y in range(bot,top) ]
                shading += [ (x,y) for x in range(leftmost_inside+1,rig) for y in [cand[leftmost_inside] .. k] ]
                elems = cand.get_elements_in_rect( [(leftmost_inside+1,bot), (rig,cand[leftmost_inside])] )
                if is_descending( graph_inv(elems) ):
                    sh_cand = cand.add_shading(shading)
                    res.append( sh_cand.add_ascending_restriction( [(leftmost_inside+1,bot), (rig,cand[leftmost_inside])] ) )

            # There is nothing in the dangerzone
            if not cand.rect_contains_elements( [(lef,bot), (rig,top)] ):
                res.append( cand.add_shading( [ (x,y) for x in range(lef,rig) for y in range(bot,top) ] ) )

            return res


    # Shaded region is in rightmost column
    if t_rig == n + 1:

        # Shaded region is below rightmost element in target
        if target[t_lef - 1] >= t_top:
            res = []

            height = cand[lef - 1]
            leftmost_popper = k
            for i,h in graph(cand)[lef:]:
                if h > height:
                    leftmost_popper = i - 1
                    break

            for i in [lef .. leftmost_popper]:
                if cand.rect_contains_elements( [(lef,bot), (i+1,top)] ) or cand.rect_contains_elements( [(i+1,bot), (k+1,top)] ):
                    continue
                sh = [ (x,y) for x in [lef .. i] for y in [height .. k+1] ]
                sh += [ (x,y) for x in [i+1 .. k+1] for y in range(bot,top) ]
                for j in [height .. k]:
                    if not cand.is_shaded(i,j):
                        m = cand.insert(i,j)
                        res.append( m.add_shading(sh) )

            sh = [ (x,y) for x in [leftmost_popper+1 .. k] for y in range(bot,top) ]
            sh += [ (x,y) for x in [lef .. leftmost_popper] for y in [height .. k] ]
            res.append( cand.add_shading(sh) )

            return res

        # the shaded region is the topmost row
        elif t_top == n + 1:
            if cand.rect_contains_elements( [(0,bot), (k+1,k+1)] ):
                return []
            return [ cand.add_shading( (x,y) for x in [0..k] for y in range(bot,top) ) ]

        # above rightmost element, but not in the topmost region
        else:
            height = cand[lef - 1]

            rightmost_above = -1
            for i,h in list(enumerate(cand))[lef - 1::-1]:
                if h > bot:
                    rightmost_above = i
                    break

            if cand.rect_contains_elements( rect ):
                return []

            sh_cand = cand.add_shading( (x,y) for x in range(lef,rig) for y in range(bot,top) )

            for i in range(lef-1, rightmost_above, -1):
                for h in range(bot,top):
                    if sh_cand.is_shaded(i,h):
                        continue
                    ins_cand = sh_cand.insert(i,h)
                    ins_cand = ins_cand.add_shading( (x,y) for x in [i+1 .. lef] for y in [bot .. top] )
                    marking = [ (x,y) for x in [i+1 .. lef] for y in [top+1 .. k+1] ]
                    if not ins_cand.is_shaded(marking):
                        res.append( ins_cand.add_marking(marking,1) )

            fin_cand = sh_cand.add_shading( (x,y) for x in range(rightmost_above+1,lef) for y in range(bot,top) )
            if rightmost_above == -1 or cand[rightmost_above] >= top:
                res.append(fin_cand)
            # TODO: Is this possible?
            else:
                marking = [ (x,y) for x in [rightmost_above+1 .. lef-1] for y in [top .. k] ]
                if not fin_cand.is_shaded(marking):
                    res.append( fin_cand.add_marking(marking,1) )

            return res


    t_l, t_r = target[t_lef - 1], target[t_rig - 1]
    c_l, c_r = cand[lef - 1], cand[rig - 1]

    # Mutual case
    if ((c_l < c_r and t_l < t_r) or (c_l > c_r and t_l > t_r)) and t_bot >= max(t_l,t_r):
        rightmost_above = -1

        for i,h in reversed( graph(cand)[:lef-1] ):
            if h > bot:
                rightmost_above = i - 1
                break

        if rightmost_above == -1 or cand[rightmost_above] >= top:

            # Subcase 1
            if not cand.rect_contains_elements( [(lef,bot), (rig,top)] ):
                res.append( cand.add_shading( (x,y) for x in range(rightmost_above+1,rig) for y in range(bot,top) ) )

            # Subcase 2
            elems = cand.get_elements_in_rect( [(lef,bot), (rig,top)] )
            if not elems:
                leftmost = rig
                bottom = bot
            else:
                leftmost,bottom = elems[0]

            if is_descending( graph_inv(elems) ):
                for i in range(lef,leftmost):
                    for j in range(bottom,top):
                        if not cand.is_shaded(i,j):
                            ins_cand = cand.insert(i,j)
                            if ins_cand.rect_contains_elements( [(i+1,j+1), (rig+1,k+2)] ):
                                continue
                            else:
                                sh = set( (x,y) for x in [rightmost_above+1 .. i] for y in [bot .. top] )
                                sh.update( set( (x,y) for x in [i+1 .. rig] for y in [j+1 .. k+1] ) )
                                res.append( ins_cand.add_shading(sh).add_ascending_restriction( [(i+1,bot), (rig+1,j+1)] ) )

                if elems:
                    if not cand.rect_contains_elements( [(leftmost,bottom), (rig,k+1)] ):
                        sh = set( (x,y) for x in range(rightmost_above+1,leftmost) for y in range(bot,top) )
                        sh.update( set( (x,y) for x in range(leftmost,rig) for y in [bottom .. k] ) )
                        res.append( cand.add_shading(sh).add_ascending_restriction( [(leftmost,bot), (rig,bottom)] ) )

        # Subcase 3
        if not cand.rect_contains_elements( [(lef,bot), (rig,k+1)] ):
            sh_cand = cand.add_shading( (x,y) for x in range(lef,rig) for y in [bot .. k] )

            for i in range(rightmost_above+1,lef):
                for j in range(bot,top):
                    if not sh_cand.is_shaded(i,j):
                        ins_cand = sh_cand.insert(i,j)
                        res.append( ins_cand.add_shading( (x,y) for x in [i+1 .. lef] for y in [bot .. top] ) )

            if rightmost_above != -1 and cand[rightmost_above] < top:
                res.append( sh_cand.add_shading( (x,y) for x in range(rightmost_above+1,lef) for y in range(bot,top) ) )


        # Subcase 4
        if not cand.rect_contains_elements( [(lef,top-1), (rig,k+1)] ):
            sh_cand = cand.add_shading( (x,y) for x in range(lef,rig) for y in [top .. k] )

            elems = cand.get_elements_in_rect( [(lef,bot), (rig,top)] )
            if not elems:
                leftmost = rig
                bottom = bot
            else:
                leftmost,bottom = elems[0]

            res_sublist = []

            if is_descending( graph_inv(elems) ):
                for i in range(lef,leftmost):
                    for j in range(bottom,top):
                        if not cand.is_shaded(i,j):
                            ins_cand = sh_cand.insert(i,j)

                            sh = set( (x,y) for x in [lef .. i] for y in [bot .. top] )
                            sh.update( set( (x,y) for x in [i+1 .. rig] for y in [j+1 .. top] ) )
                            res_cand = ins_cand.add_shading(sh).add_ascending_restriction( [(i+1,bot), (rig+1,j+1)] )
                            res_sublist.append( (res_cand, j+1, top+1) )


                if elems:
                    sh = set( (x,y) for x in range(lef,leftmost) for y in range(bot,top) )
                    sh.update( set( (x,y) for x in range(leftmost,rig) for y in range(bottom,top) ) )
                    res_cand = sh_cand.add_shading(sh).add_ascending_restriction( [(leftmost,bot), (rig,bottom)] )
                    res_sublist.append( (res_cand, bottom , top) )


                for patt, height, new_top in res_sublist:
                    for i in range(rightmost_above+1,lef):
                        for j in range(height,new_top):
                            if not patt.is_shaded(i,j):
                                ins_patt = patt.insert(i,j)
                                res.append( ins_patt.add_shading( (x,y) for x in [i+1 .. lef] for y in range(bot,new_top+1) ) )

                    if height < patt[rightmost_above] < new_top:
                        res.append( patt.add_shading( (x,y) for x in range(rightmost_above+1,lef) for y in range(bot,new_top) ) )

        # Subcase 5 (does not apply if the shaded region in target is in the topmost row)
        if not cand.rect_contains_elements( [(lef,bot), (rig,top)] ) and t_top != n + 1:
            sh_cand = cand.add_shading( (x,y) for x in range(lef,rig) for y in range(bot,top) )

            for i in range(rightmost_above+1, lef):
                for j in range(bot,top):
                    if not sh_cand.is_shaded(i,j):
                        ins_cand = sh_cand.insert(i,j)
                        mp_cand = ins_cand.add_shading( (x,y) for x in [i+1 .. lef] for y in [bot .. top] )
                        mark = [ (x,y) for x in [i+1 .. lef] for y in [top+1 .. k+1] ]
                        if not mp_cand.is_shaded(mark):
                            res.append( mp_cand.add_marking(mark,1) )

            if rightmost_above != -1 and cand[rightmost_above] < top:
                mp_cand = ins_cand.add_shading( (x,y) for x in [rightmost_above+1 .. lef] for y in [bot .. top] )
                mark = [ (x,y) for x in range(rightmost_above,lef) for y in [top .. k] ]
                if not mp_cand.is_shaded(mark):
                    res.append( mp_cand.add_marking(mark,1) )

        # Subcase 6 (does not apply if the shaded region in target is in the topmost row)
        if t_top != n + 1:
            elems = cand.get_elements_in_rect( [(lef,bot), (rig,top)] )
            if not elems:
                leftmost = rig
                bottom = bot
            else:
                leftmost,bottom = elems[0]

            res_sublist = []

            if is_descending( graph_inv(elems) ):
                for i in range(lef,leftmost):
                    if not cand.rect_contains_elements( [(i+1,top-1), (rig,k+1)] ):
                        for j in range(bottom,top):
                            if not cand.is_shaded(i,j):
                                ins_cand = cand.insert(i,j)

                                sh = [ (x,y) for x in [lef .. i] for y in [bot .. top] ]
                                sh += [ (x,y) for x in [i+1 .. rig] for y in [j+1 .. k+1] ]
                                res_cand = ins_cand.add_shading(sh).add_ascending_restriction( [(i+1,bot), (rig+1,j+1)] )
                                res_sublist.append( (res_cand, top+1) )



                if elems and not cand.rect_contains_elements( [(leftmost,bottom), (rig,k+1)] ):
                    sh = [ (x,y) for x in range(lef,leftmost) for y in range(bot,top) ]
                    sh += [ (x,y) for x in range(leftmost,rig) for y in [bottom .. k] ]
                    res_cand = cand.add_shading(sh).add_ascending_restriction( [(leftmost,bot), (rig,bottom)] )
                    res_sublist.append( ( res_cand, top) )


                for patt, new_top in res_sublist:
                    for i in range(rightmost_above+1,lef):
                        for j in range(bot,new_top):
                            if not patt.is_shaded(i,j):
                                ins_patt = patt.insert(i,j)
                                dp_cand = ins_patt.add_shading( (x,y) for x in [i+1 .. lef] for y in range(bot,new_top+1) )
                                mark = [ (x,y) for x in [i+1 .. lef] for y in [new_top+1 .. len(dp_cand)] ]
                                if not dp_cand.is_shaded(mark):
                                    res.append( dp_cand.add_marking(mark,1) )

                    if rightmost_above != -1 and cand[rightmost_above] < top:
                        dp_cand = patt.add_shading( (x,y) for x in range(rightmost_above+1,lef) for y in range(bot,new_top) )
                        mark = [ (x,y) for x in range(rightmost_above+1,lef) for y in [new_top .. len(dp_cand)] ]
                        if not dp_cand.is_shaded(mark):
                            res.append( dp_cand.add_marking(mark,1) )

        return res

    # Case 1
    # Ascent in target and descent in candidate
    if c_l > c_r and t_l < t_r:

        # Shaded region below descent
        if t_top <= t_l:
            right_popper = k
            left_popper = k
            for i,h in graph(cand)[rig:]:
                if h > c_l:
                    left_popper = i - 1
                    if right_popper == k:
                        right_popper = i - 1
                    break
                elif h > c_r and right_popper != k:
                    right_popper = i - 1

            # I right_popper <= left_popper

            for i in [rig .. right_popper]:
                for j in [c_l .. k]:
                    if not cand.is_shaded(i,j):
                        ins_cand = cand.insert(i,j)
                        sh = [ (x,y) for x in [rig .. i] for y in [c_r .. len(ins_cand)] ]
                        res.append( ins_cand.add_shading(sh) )

            if right_popper == left_popper:
                sh = [ (x,y) for x in [rig .. right_popper] for y in [c_r .. k] ]
                res.append( cand.add_shading(sh) )

            sub_res = []
            for i in [rig .. right_popper]:
                for j in range(c_r,c_l):
                    if not cand.is_shaded(i,j):
                        ins_cand = cand.insert(i,j)
                        sh_cand = ins_cand.add_shading( [ (x,y) for x in [rig .. i] for y in [c_r .. len(ins_cand)] ] )
                        sub_res.append( (sh_cand, i+1, left_popper+1, c_l+1) )

            if right_popper < left_popper:
                sh_cand = cand.add_shading( [ (x,y) for x in [rig .. right_popper] for y in [c_r .. k] ] )
                sub_res.append( (sh_cand, right_popper+1, left_popper, c_l) )

            for patt, new_right_popper, new_left_popper, new_c_l in sub_res:
                for i in [new_right_popper .. new_left_popper]:
                    for j in [new_c_l .. len(patt)]:
                        if not patt.is_shaded(i,j) and not patt.rect_contains_elements( [(new_right_popper,bot), (i,top)] ):
                            ins_patt = patt.insert(i,j)
                            sh = [ (x,y) for x in [new_right_popper .. i] for y in [bot .. top-1] + [new_c_l .. len(ins_patt)] ]
                            sh_patt = ins_patt.add_shading(sh)
                            res.append(sh_patt)

                if not patt.rect_contains_elements( [(new_right_popper,bot), (new_left_popper+1,top)] ):
                    sh = [ (x,y) for x in [new_right_popper .. new_left_popper] for y in [bot .. top-1] + [new_c_l .. len(patt)] ]
                    sh_patt = patt.add_shading(sh)
                    res.append(sh_patt)

            sub_patt = cand.get_elements_in_rect( [(lef,bot), (rig,top)] )
            rightmost_inside = lef
            if sub_patt:
                rightmost_inside,_ = sub_patt[-1]

            return res

        # Shaded region is above the descent
        elif t_bot >= t_r:
            return [ MeshPattern(cand) ]

        # Shaded region is between the elements in the descent
        else:
            left_popper = k
            for i,h in graph(cand)[rig:]:
                if h > c_l:
                    left_popper = i - 1
                    break

            sub_res = []

            for i in [rig .. left_popper]:
                for j in [c_l .. k]:
                    if not cand.is_shaded(i,j):
                        ins_cand = cand.insert(i,j)
                        if not cand.rect_contains_elements( [(rig,bot), (i+1,top)] ):
                            sub_res.append( ins_cand.add_shading( [ (x,y) for x in [rig .. i] for y in [bot .. (top-1)] + [c_l .. len(ins_cand)] ] ) )

            if not cand.rect_contains_elements( [(rig,bot), (left_popper+1,top)] ):
                sub_res.append( cand.add_shading( [ (x,y) for x in [rig .. left_popper] for y in [bot .. top-1] + [c_l .. len(cand)] ] ) )

            if y == c_l:
                return sub_res

            rightmost_inside = lef
            for i,h in graph(cand)[lef:rig-1:-1]:
                if bot < h < top:
                    rightmost_inside = i
                    break

            for patt in sub_res:
                for i in range(rightmost_inside,rig):
                    for j in range(bot,top):
                        if not patt.is_shaded(i,j):
                            ins_patt = patt.insert(i,j)
                            mark = [ (x,y) for x in [i+1 .. rig] for y in [top+1 .. c_l+1] ]
                            sh = [ (x,y) for x in [i+1 .. rig] for y in [bot .. top] ]
                            if not ins_patt.is_shaded(mark):
                                res.append( ins_patt.add_shading(sh).add_marking(mark,1) )

                if rightmost_inside == lef:
                    res.append( patt.add_shading( [ (x,y) for x in range(lef,rig) for y in range(bot,top) ] ) )

            return res

    # Case 2
    # Descent in target and descent in candidate
    if c_l > c_r and t_l > t_r:

        # Shaded region below descent
        if t_top <= t_r:
            left_popper = 0
            for i,h in graph(cand)[lef:rig]:
                if h > c_l:
                    left_popper = i - 1
                    break
            else:
                raise Exception, "Left popper missing"

            right_popper_ind = k
            right_popper_height = c_r
            for i,h in graph(cand)[rig:]:
                if h > c_r:
                    right_popper_ind, right_popper_height = i - 1, h
                    break

            sub_res = []

            for i in [rig .. right_popper_ind]:
                for j in [right_popper_height .. k]:
                    if not cand.is_shaded(i,j):
                        ins_cand = cand.insert(i,j)
                        if not ins_cand.rect_contains_elements( [(i,bot), (right_popper_ind+1,top)] ):
                            # TODO: This is incorrect
                            #sh = [ (x,y) for x in [i .. right_popper_ind] for y in [bot .. top-1] + [c_r .. k+1] ]
                            sh = [ (x,y) for x in [rig .. i] for y in [bot .. top-1] + [c_r .. k+1] ]
                            sub_res.append( ins_cand.add_shading(sh) )


            if not cand.rect_contains_elements( [(rig,bot), (right_popper_ind+1,top)] ):
                sh = [ (x,y) for x in [rig .. right_popper_ind] for y in [bot .. top-1] + [c_r .. k] ]
                sub_res.append( cand.add_shading(sh) )

            for patt in sub_res:
                for i in [lef .. left_popper]:
                    for j in [patt[lef-1] .. len(patt)]:
                        if not patt.is_shaded(i,j):
                            ins_patt = patt.insert(i,j)
                            sh = [ (x,y) for x in [lef .. i] for y in [ins_patt[lef-1] .. len(ins_patt)] ]
                            sh += [ (x,y) for x in [i+1 .. rig] for y in range(bot,top) ]
                            res.append( ins_patt.add_shading(sh) )

                sh = [ (x,y) for x in [lef .. left_popper] for y in [patt[lef-1] .. len(patt)] ]
                sh += [ (x,y) for x in range(left_popper+1,rig) for y in range(bot,top) ]
                res.append( patt.add_shading(sh) )

            return res

        # Shaded region is between elements in descent
        else:
            left_popper = 0
            for i,h in graph(cand)[lef:rig]:
                if h > c_l:
                    left_popper = i - 1
                    break
            else:
                raise Exception, "Left popper missing"

            sub_res = []

            for i in [lef .. left_popper]:
                for j in [cand[lef-1] .. k]:
                    if not cand.is_shaded(i,j):
                        ins_cand = cand.insert(i,j)
                        sh = [ (x,y) for x in [lef .. i] for y in [ins_cand[lef-1] .. len(ins_cand)] ]
                        sub_res.append( (ins_cand.add_shading(sh), i+1, rig+1) )

            sh = [ (x,y) for x in [lef .. left_popper] for y in [cand[lef-1] .. k] ]
            sub_res.append( (cand.add_shading(sh), left_popper + 1, rig) )

            for patt, left, right in sub_res:
                sub_patt = patt.get_elements_in_rect( [(left,bot), (right,top)] )
                if is_descending( graph_inv(sub_patt) ):
                    leftmost_ind, leftmost_height = right, bot
                    if sub_patt:
                        leftmost_ind, leftmost_height = sub_patt[0]

                    if patt.rect_contains_elements( [(leftmost_ind,top-1), (right,len(patt)+1)] ):
                        continue

                    for i in range(left,leftmost_ind):
                        for j in range(leftmost_height,top):
                            if not patt.is_shaded(i,j):
                                ins_patt = patt.insert(i,j)
                                if ins_patt.rect_contains_elements( [(i+1,top-1), (right+1,len(ins_patt)+1)] ):
                                    continue
                                sh = [ (x,y) for x in [left .. i] for y in [bot .. top] ]
                                sh += [ (x,y) for x in [i+1 .. right] for y in [j+1 .. len(ins_patt)] ]
                                sh_patt = ins_patt.add_shading(sh)
                                res.append( sh_patt.add_ascending_restriction( [(i+1,bot), (right+1,j+1)] ) )

                sh = [ (x,y) for x in range(left,leftmost_ind) for y in range(bot,top) ]
                sh += [ (x,y) for x in range(leftmost_ind+1,right) for y in [leftmost_height .. len(patt)] ]
                sh_patt = patt.add_shading(sh)
                res.append( sh_patt.add_ascending_restriction( [(leftmost_ind,bot), (right,leftmost_height)] ) )

            return res

    # Case 3
    # Ascent in target and candidate
    if c_l < c_r and t_l < t_r:

        # Shaded region is below ascent
        if t_top <= t_l:
            right_popper = k
            for i,h in graph(cand)[rig:]:
                if h > c_r:
                    right_popper = i - 1
                    break

            left_popper = 0
            for i,h in graph(cand)[lef:rig]:
                if h > c_l:
                    left_popper = i - 1
                    break
            else:
                raise Exception, "Left popper missing"

            sub_res = []

            for i in [rig .. right_popper]:
                for j in [c_r .. k]:
                    if not cand.is_shaded(i,j):
                        ins_cand = cand.insert(i,j)
                        if ins_cand.rect_contains_elements( [(rig,bot), (i+1,top)] ):
                            continue
                        sh = [(x,y) for x in [rig .. i] for y in [bot .. top-1] + [c_r .. k+1] ]
                        sub_res.append( ins_cand.add_shading(sh) )

            if not cand.rect_contains_elements( [(rig,bot), (right_popper+1,top)] ):
                sh = [(x,y) for x in [rig .. right_popper] for y in [bot .. top-1] + [c_r .. k] ]
                sub_res.append( cand.add_shading(sh) )


            for patt in sub_res:
                for i in [lef .. left_popper]:
                    for j in [patt[lef-1] .. len(patt)]:
                        if not patt.is_shaded(i,j) and not patt.rect_contains_elements( [(i+1,bot), (rig,top)] ):
                            ins_patt = patt.insert(i,j)
                            sh = [ (x,y) for x in [lef .. i] for y in [ins_patt[lef-1] .. len(ins_patt)] ]
                            sh += [ (x,y) for x in [i+1 .. rig] for y in range(bot,top) ]
                            res.append( ins_patt.add_shading(sh) )
                if not patt.rect_contains_elements( [(left_popper+1,bot),(rig,top)] ):
                    sh = [ (x,y) for x in [lef .. left_popper] for y in [patt[lef-1] .. len(patt)] ]
                    sh += [ (x,y) for x in range(left_popper+1,rig) for y in range(bot,top) ]
                    res.append( patt.add_shading(sh) )

            return res

        # Shaded region is between the elements in the ascent
        else:
            if cand.rect_contains_elements( [(lef,bot), (rig,top)] ):
                return []

            sh_cand = cand.add_shading( [(x,y) for x in range(lef,rig) for y in range(bot,top) ] )

            rightmost_above = -1
            for i,h in list(enumerate(cand))[lef - 1::-1]:
                if h > bot:
                    rightmost_above = i
                    break

            right_popper = k
            for i,h in graph(cand)[rig:]:
                if h > c_r:
                    right_popper = i - 1
                    break

            sub_res = []

            for i in [rig .. right_popper]:
                for j in [c_r .. k]:
                    if not sh_cand.is_shaded(i,j):
                        ins_cand = sh_cand.insert(i,j)
                        if ins_cand.rect_contains_elements( [(rig,bot), (i+1,top)] ):
                            continue
                        sh = [(x,y) for x in [rig .. i] for y in [bot .. top-1] + [c_r .. k+1] ]
                        sub_res.append( ins_cand.add_shading(sh) )

            if not sh_cand.rect_contains_elements( [(rig,bot), (right_popper+1,top)] ):
                sh = [(x,y) for x in [rig .. right_popper] for y in [bot .. top-1] + [c_r .. k] ]
                sub_res.append( sh_cand.add_shading(sh) )


            for patt in sub_res:
                if rightmost_above == -1 or cand[rightmost_above] >= top:
                    res.append( patt.add_shading( [ (x,y) for x in range(rightmost_above+1,rig) for y in range(bot,top) ] ) )

                for i in range(rightmost_above+1,lef):
                    for j in range(bot,top):
                        if not patt.is_shaded(i,j):
                            ins_patt = patt.insert(i,j)
                            mark = [ (x,y) for x in [i+1 .. lef] for y in [top+1 .. len(ins_patt)] ]
                            sh = [ (x,y) for x in [i+1 .. lef] for y in [bot .. top] ]
                            if not ins_patt.is_shaded(mark):
                                res.append( ins_patt.add_shading(sh).add_marking(mark,1) )

                if rightmost_above != -1 and cand[rightmost_above] < top:
                    mark = [ (x,y) for x in range(rightmost_above+1,lef) for y in [top .. len(patt)] ]
                    sh = [ (x,y) for x in range(rightmost_above+1,lef) for y in range(bot,top) ]
                    if not patt.is_shaded(mark):
                        res.append( patt.add_shading(sh).add_marking(mark,1) )

            return res
