per = Permutations(25).random_element()
print per
time m.avoided_by(per)
time m.old_avoided_by(per)

m = None
Perm = None
def do_random_test(n, permlen, pattlen, shade_prob):
	global Perm
	global m
	old_time = 0
	new_time = 0
	for i in xrange(1,n+1):
		P = Permutations(randint(*pattlen)).random_element()
		m = MeshPattern(P, [(x,y) for x in [0..len(P)] for y in [0..len(P)] if random() < shade_prob])
		Perm = Permutations(randint(*permlen)).random_element()
		new_time += timeit('m.avoided_by(Perm)', repeat=1, seconds=True)
		old_time += timeit('m.old_avoided_by(Perm)', repeat=1, seconds = True)
		if m.avoided_by(Perm) != m.old_avoided_by(Perm):
			print "Foookkk!!"
			return
		if i % 10000 == 0:
			print "Average old time", old_time / (i+1)
			print "Average new time", new_time / (i+1)
	print "FINAL"
	print "Average old time", old_time / n
	print "Average new time", new_time / n

m = None
Perm = None
def do_random_test2(permlen, pattlen, shade_prob):
	global Perm
	global m
	old_time = 0
	new_time = 0
	P = Permutations(pattlen).random_element()
	m = MeshPattern(P, [(x,y) for x in [0..pattlen] for y in [0..pattlen] if random() < shade_prob])
	for Perm in Permutations(permlen):
		new_time += timeit('m.avoided_by(Perm)', repeat=1, seconds=True)
		old_time += timeit('m.old_avoided_by(Perm)', repeat=1, seconds = True)
	print "FINAL"
	print "Average old time", old_time
	print "Average new time", new_time

def do_random_test_new(permlen, patt):
	for Perm in Permutations(permlen):
		patt.avoided_by(Perm)

def do_random_test_old(permlen, patt):
	for Perm in Permutations(permlen):
		patt.old_avoided_by(Perm)

P = Permutations(5).random_element()
m = MeshPattern(P, [(x,y) for x in [0..len(P)] for y in [0..len(P)] if random() < 0.5])

n = 7

time do_random_test_new(n, m)
time do_random_test_old(n, m)
