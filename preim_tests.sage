def test_correctness(target, sop, inv_class, n):
    corr = correct_perms(target, sop, n)
    hypo = hypo_perms(inv_class, n)
    if corr == hypo:
        return True
    print 'correct:'
    print corr
    print 'obtained:'
    print hypo

def correct_perms(patt, sop, n):
    return sorted( p for p in Permutations(n) if patt.avoided_by(sop(p)) )

def avoids_all(perm, patts):
    return all( patt.avoided_by(perm) for patt in patts )

def hypo_perms(inv_class, n):
    return sorted( perm for perm in Permutations(n) if avoids_all(perm, inv_class) )


def test_preimI(patt, n, exclude=True):
    return test_correctness(ClassicalPattern(patt),
            insertion_sort, preimI(ClassicalPattern(patt), exclude), n)

def test_preimT(patt, n, exclude=True):
    return test_correctness(ClassicalPattern(patt),
            pop_stack_sort, preimT(ClassicalPattern(patt), exclude), n)

def test_preimQ(patt, n, exclude=True):
    return test_correctness(ClassicalPattern(patt),
            queue_sort, preimQ(ClassicalPattern(patt), exclude), n)

def test_preimSd(d, patt, n, exclude=True):
    return test_correctness(ClassicalPattern(patt),
            lambda x: stack_sort(x, d), preimSd(d, ClassicalPattern(patt), exclude), n)

def test_preimS2(patt, n, exclude=True):
    return test_preimSd(2, patt, n, exclude)

def test_preimS3(patt, n, exclude=True):
    return test_preimSd(3, patt, n, exclude)

def test_preimS4(patt, n, exclude=True):
    return test_preimSd(4, patt, n, exclude)

def test_preim(preim_test, patt_len, perm_len, report=False, exclude=True):
    if report:
        print 'Testing', preim_test.__name__
        print 'Patterns of length', patt_len
        print 'Permutations of length up to', perm_len

    for p in Permutations(patt_len):
        if report:
            print p
        for i in range(perm_len + 1):
            if not preim_test(p,i,exclude):
                if report:
                    print 'TEST FAILED'
                return False
    return True

tests = [ test_preimS2,
        test_preimS3,
        test_preimS4,
        test_preimQ,
        test_preimT,
        test_preimI ]

def test_all(patt_len, perm_len, report=False, exclude=True):
    for preim in tests:
        for k in [2 .. patt_len]:
            test_preim(preim, k, perm_len, report, exclude)

