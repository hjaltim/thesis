def pairs(w):
    """
    Returns a tuple (inv, ninv) where inv contains all the inverions
    of w and ninv contains all the non-inversions of w.
    """
    inv  = []
    ninv = []
    for i,u in enumerate(w):
        for v in w[i+1:]:
            if u > v:
                inv.append((u,v))
            else:
                ninv.append((u,v))
    return inv, ninv

#TODO: Do this more efficiently
def gcand(word):
    """
    General candidate function.
    """
    target_inv = Set(pairs(word)[0])
    return [ p for p in Permutations(len(word)) if target_inv.issubset(Set(pairs(p)[0])) ]


################################################################################
#                         Implementation of preimS_d                           #
################################################################################

def candS(d,w):
    """
    Candidates for S_d.
    """
    if d == 1:
        return [w]
    if w:
        w = w[:]
        n = max(w)
        i = w.index(n)
        w.pop(i)
        return [ x + [n] + y for j in range(i+1) for x in candS(d,w[:j]) for y in candS(d-1,w[j:]) ]
    else:
        return [[]]

#
#    r2 | r1 |
#    ---x----|---
#       |    |
#    ---|----x---
#       |    |
#
def decorateSd(d, patt, cand, exclude=True):
    res = [ DecoratedPattern(cand) ]
    n = len(cand)
    patt_inversions, _ = pairs(patt)
    inv_cand = Permutation(cand).inverse()

    vd = MeshPattern( range(d-1,0,-1), [ (i,j) for i in range(d) for j in range(d) if (d - 1 - i) < j ] )

    for (i,j) in pairs(cand)[0]:
        region1 = set( [ (a,b)
            for a in [inv_cand(i) .. inv_cand(j) - 1]
            for b in [i .. n] ] )
        region2 = set( [ (a,b)
            for a in [0 .. inv_cand(i) - 1]
            for b in [i .. n] ] )

        new_res = []
        for r in res:
            if (i,j) in patt_inversions:
                # Meta pattern C_1
                if r.can_mark(region1):
                    if r.element_in_region(region1):
                        new_res.append(r)
                    else:
                        new_res.append(r.add_marking(region1.difference(r.shaded_boxes), 1))

                # Meta pattern C_2
                if r.can_shade(region1) and r.can_add_co(region2, vd):
                    s = r.add_co_decoration(region2,vd)
                    if exclude:
                        s = s.add_shading(region1)
                    new_res.append(s)

            # Metapattern C_3
            elif r.can_shade(region1) and r.can_add_av(region2, vd):
                new_res.append( r.add_shading(region1)
                        .add_av_decoration(region2, vd) )

        res = new_res

    return res


def preimSd(d, target, exclude=True):
    return sum( [ decorateSd(d, target, p, exclude) for p in candS(d + 1, target)], [] )


################################################################################
#                          Implementation of preimQ                            #
################################################################################

#
#    r2 | r1 |
#    ---x----|---
#       |    |
#    ---|----x---
#       |    |
#
def decorateQ(patt, cand, exclude=True):
    res = [ DecoratedPattern(cand) ]
    n = len(cand)
    patt_inversions, _ = pairs(patt)
    inv_cand = Permutation(cand).inverse()
    two_one = ClassicalPattern([2,1])

    for (i,j) in pairs(cand)[0]:
        region1 = set( [ (a,b)
            for a in [inv_cand(i) .. inv_cand(j) - 1]
            for b in [i .. n] ] )
        region2 = set( [ (a,b)
            for a in [0 .. inv_cand(i) - 1]
            for b in [i .. n] ] )

        new_res = []
        for r in res:
            if (i,j) in patt_inversions:
                # Meta pattern Q_1
                if r.can_mark(region2):
                    if r.element_in_region(region2):
                        new_res.append(r)
                    else:
                        new_res.append( r.add_marking(
                            region2.difference(r.shaded_boxes), 1) )

                # Meta pattern Q_2
                if r.can_shade(region2) and r.can_add_co(region1, two_one):
                    s = r.add_co_decoration(region1, two_one)
                    if exclude:
                        s = s.add_shading(region2)
                    new_res.append(s)

            # Meta pattern Q_3
            elif r.can_shade(region2) and r.can_add_av(region1, two_one):
                new_res.append( r.add_shading(region2)
                        .add_av_decoration(region1, two_one) )

        res = new_res
    return res

def preimQ(target, exclude=True):
    return sum( [ decorateQ(target, p, exclude) for p in gcand(target) ], [] )


################################################################################
#                          Implementation of preimT                            #
################################################################################

#
#       | r1 |
#    ---x----|---
#       | r2 |
#    ---|----x---
#       | r3 |
def decorateT(patt, cand, exclude=True):
    res = [ DecoratedPattern(cand) ]
    n = len(cand)
    patt_inversions, _ = pairs(patt)
    inv_cand = Permutation(cand).inverse()
    one_two = ClassicalPattern([1,2])

    for (i,j) in pairs(cand)[0]:
        region1 = set( [ (a,b)
            for a in [inv_cand(i) .. inv_cand(j) - 1]
            for b in [i .. n] ] )
        region2 = set( [ (a,b)
            for a in [inv_cand(i) .. inv_cand(j) - 1]
            for b in [j .. i - 1] ] )
        region3 = set( [ (a,b)
            for a in [inv_cand(i) .. inv_cand(j) - 1]
            for b in [0 .. j - 1] ] )

        new_res = []
        for r in res:
            if (i,j) in patt_inversions:
                # Meta pattern T_1
                if r.can_mark(region1):
                    if r.element_in_region(region1):
                        new_res.append(r)
                    else:
                        new_res.append( r.add_marking(region1
                            .difference(r.shaded_boxes), 1) )

                # Meta pattern T_2
                if r.can_shade(region1) and r.can_mark(region3):
                    if r.element_in_region(region3):
                        if exclude:
                            new_res.append( r.add_shading(region1) )
                        else:
                            new_res.append(r)
                    else:
                        s = r.add_marking(region3.difference(r.shaded_boxes), 1)
                        if exclude:
                            s = s.add_shading(region1)
                        new_res.append(s)

                # Meta pattern T_3
                if (r.can_shade(region1) and r.can_shade(region3)
                        and r.can_add_co(region2, one_two)):
                    s = r.add_co_decoration(region2, one_two)
                    if exclude:
                        s = s.add_shading(region1).add_shading(region3)
                    new_res.append(s)

            # Meta pattern T_4
            elif (r.can_shade(region1) and r.can_shade(region3)
                    and r.can_add_av(region2, one_two)):
                new_res.append( r.add_shading(region1)
                        .add_shading(region3)
                        .add_av_decoration(region2, one_two) )

        res = new_res
    return res

def preimT(target, exclude=True):
    return sum( [ decorateT(target, p, exclude) for p in gcand(target) ], [] )


################################################################################
#                          Implementation of preimI                            #
################################################################################

#
#    r1 | r4 |
#    ---x----|---
#       |    |
#    r2 | r3 x---
#       |    |
#
def decorateI(patt, cand, exclude=True):
    res = [ DecoratedPattern(cand) ]
    n = len(cand)
    patt_inversions, _ = pairs(patt)
    inv_cand = Permutation(cand).inverse()
    two_one = ClassicalPattern([2,1])

    for (i,j) in pairs(cand)[0]:
        region1 = set( [ (a,b) for a in [0 .. inv_cand(i) - 1]
            for b in [i .. n] ] )
        region2 = set( [ (a,b) for a in [0 .. inv_cand(i) - 1]
            for b in [0 .. i - 1] ] )
        region3 = set( [ (a,b) for a in [inv_cand(i) .. inv_cand(j) - 1]
            for b in [0 .. i - 1] ] )
        region4 = set( [ (a,b) for a in [inv_cand(i) .. inv_cand(j) - 1]
            for b in [i .. n] ] )

        new_res = []
        for r in res:
            if (i,j) in patt_inversions:
                # Meta pattern I_1
                if r.can_mark(region1):
                    if r.element_in_region(region1):
                        new_res.append(r)
                    else:
                        new_res.append( r.add_marking(region1
                            .difference(r.shaded_boxes), 1) )

                # Meta pattern I_2
                if (r.can_shade(region1) and r.can_add_co(region2, two_one)):
                    s = r.add_co_decoration(region2, two_one)
                    if exclude:
                        s = s.add_shading(region1)
                    new_res.append(s)

                # Meta pattern I_3
                if (r.can_shade(region1) and
                        r.can_add_av(region2, two_one) and
                        r.can_mark(region3)):
                    if r.element_in_region(region3):
                        if exclude:
                            new_res.append( r.add_shading(region1)
                                    .add_av_decoration(region2, two_one) )
                        else:
                            new_res.append(r)
                    else:
                        s = r.add_marking(region3, 1)
                        if exclude:
                            s = s.add_shading(region1).add_av_decoration(region2, two_one)
                        new_res.append(s)

                # Meta pattern I_4
                if (r.can_shade(region1) and r.can_shade(region3) and
                        r.can_add_av(region2, two_one) and
                        r.can_add_co(region4, two_one)):
                    s = r.add_co_decoration(region4, two_one)
                    if exclude:
                        s = (s.add_shading(region1)
                                .add_shading(region3)
                                .add_av_decoration(region2, two_one))
                    new_res.append(s)

            # Meta pattern I_5
            elif (r.can_shade(region1) and r.can_shade(region3) and
                    r.can_add_av(region2, two_one) and
                    r.can_add_av(region4, two_one)):
                new_res.append( r.add_shading(region1)
                        .add_shading(region3)
                        .add_av_decoration(region2, two_one)
                        .add_av_decoration(region4, two_one) )

        res = new_res
    return res

def preimI(target, exclude=True):
    return sum( [ decorateI(target, p, exclude) for p in gcand(target) ], [] )
