def stack_sort(w, d = Infinity):
    res = []
    stack = []
    for a in w:
        if not stack:
            stack.append(a)
        elif stack[-1] > a:
            if len(stack) < d - 1:
                stack.append(a)
            else:
                res.append(a)
        else:
            while stack and stack[-1] < a:
                res.append(stack.pop())
            stack.append(a)
    return res + stack[::-1]

def bubble_sort(w):
    return stack_sort(w,2)

def stack_sort_rec(k,perm):
    if not perm:
        return []

    if not k:
        return perm

    i = loc_max(perm)

    return stack_sort_rec(k,perm[:i]) + stack_sort_rec(k-1,perm[i+1:]) + [perm[i]]

def leftMaxima(w):
	L = w[:1]
	for x in w[1:]:
		if x > L[-1]:
			L.append(x)
	return L

def merge(u,v):
	if not u: return v
	if not v: return u
	if u[0] < v[0]:
		return u[:1] + merge(u[1:], v)
	else:
		return v[:1] + merge(u, v[1:])

def queue_sort(w):
	u = leftMaxima(w)
	v = w[:]
	for x in u:
		v.remove(x)
	return merge(u,v)

def insertion_sort(w):
    if len(w) < 2:
        return w

    last = w[0]
    for i in range(1,len(w)):
        if w[i] < last:
            ind = i
            break
        last = w[i]
    else:
        return w

    new = w[:ind] + w[ind+1:]

    for i,a in enumerate(new):
        if a > w[ind]:
            return new[:i] + [w[ind]] + new[i:]

def pancake_sort(w):
    if len(w) < 2:
        return w

    i = len(w) - 1
    while i >= 0 and w[i] == i + 1:
        i -= 1

    if i < 0:
        return w


    ind = w.index(i+1)

    return w[ind+1:i+1][::-1] + w[:ind] + [i+1] + w[i+1:]

def pop_stack_sort(w):
    res = []
    stack = []
    for a in w:
        if not stack:
            stack.append(a)
        elif stack[-1] > a:
            stack.append(a)
        else:
            while stack:
                res.append(stack.pop())
            stack.append(a)
    return res + stack[::-1]




